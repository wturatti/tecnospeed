<!DOCTYPE html>
<html>
  <head>
    <title>Usuario</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../../css/usuario.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/usuario.js"></script>
  </head>
  <body>
	<div id="loading_usuario">
		<img src="../../images/carregando.gif" />
	</div>
	<div id="content_usuario">
		<h1>Usuário</h1>
		<fieldset>
			<legend>Cadastro</legend>
			<form id="form_usuario" name="form_usuario">
				<label>Nome: </label> <input type="text" id="nome" name="nome" /> <br>
				<label>Email: </label> <input type="email" id="email" name="email" /> <br>
				<label>Senha: </label> <input type="password" id="senha" name="senha" /> <br> <br>
				<input type="button" id="btn_cadastrar" name="btn_cadastrar" value="Salvar" />			
			</form>
		</fieldset>
		<br><br>
		<input type="button" id="btn_atualizar" name="btn_atualizar" value="Atalizar lista" />			
		<fieldset>
			<legend>Usuários cadastrados</legend>
			<table id="table_usuario" name="table_usuario"></table>
		</fieldset>	
	</div>
  </body>
</html>