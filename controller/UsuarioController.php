<?php	
	include_once dirname(__FILE__) . "/../model/Conexao.class.php";
	include_once dirname(__FILE__) . "/../model/Usuario.class.php";
	$usuario = new Usuario();
	$con = new Conexao();
	
	try {
		if (!isset($_REQUEST['acao']))
			throw new Exception("Parametro acao nao informado.");
		
		$response = array('success' => true);
		$acao = $_REQUEST['acao'];
		
		switch ($acao){
			case 'listar':
				$rows = $usuario->listar();
				$response['data'] = $rows;
				
				break;
			case 'listarid':
				if (!isset($_REQUEST['id']))
					throw new Exception("ID nao informado.");
				
				$id = $_REQUEST['id'];
				
				$usuario->getId($id);
				echo "ID: " . $usuario->id . "<br>";
				echo "Nome: " . $usuario->nome . "<br>";
				echo "Email: " . $usuario->email . "<br>";
				
				break;
			case 'cadastrar':
				if (!isset($_REQUEST['nome']) && !isset($_REQUEST['email']) && !isset($_REQUEST['senha']))
					throw new Exception("Ação inválida.");
				
				$nome = $_REQUEST['nome'];
				$email = $_REQUEST['email'];
				$senha = $_REQUEST['senha'];
				
				$result = $con->_conexao->query("INSERT INTO usuario (nome, email, senha) VALUES ('" . $nome . "', '" . $email . "', '" . md5($senha) . "')");
				
				break;
			case 'excluir':
				if (!isset($_REQUEST['id']))
					throw new Exception("Ação inválida.");
				
				$id = $_REQUEST['id'];
				
				$query = "DELETE FROM usuario WHERE id = " . $id . ";";
				
				if (!$usuario->excluir($query))
					throw new Exception("Erro ao excluir usuário.");
				
				$rows = $usuario->listar();
				$response['data'] = $rows;
				
				break;
		}
		
	} catch(Exception $e){
		$response['success'] = false;
		$response['error'] = $e->getMessage();
	}
	
	echo json_encode($response);