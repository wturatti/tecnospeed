$(function(){

    'use strict';

    var Usuario = {
        vars: {
			
		},
		init: function(){
			var self = this;
			
			self.listar();
			
			$("#btn_cadastrar").click(function(){
				if (self.validaFormulario()){
					self.cadastrar();
				}
				
				return false;
			});
			
			$("#btn_atualizar").click(function(){
				self.listar();					
								
				return false;
			});
			
        },
		validaFormulario: function(){
			var self = this,
				nome = $("#nome").val(),
				email = $("#email").val(),
				senha = $("#senha").val();
			
			if (nome.length == 0){
				alert("Informe o nome do usuário.");
				return false;
			} else if (email.length == 0){
				alert("Informe o e-mail do usuário.");
				return false;
			} else if (senha.length == 0){
				alert("Informe a senha do usuário.");
				return false;
			}			
			
			return true;
		},
		limparFormulario: function(){
			var self = this;
			
			$("#form_usuario")[0].reset();
		},
		cadastrar: function(){
			var self = this;
			
			self.exibirLoading();
			
			$.ajax({
				url: '/tecnospeed/controller/UsuarioController.php',
				data: {
					acao: 'cadastrar',
					nome: $("#nome").val(),
					email: $("#email").val(),
					senha: $("#senha").val()
				},
				method: 'POST',
				dataType: 'json',
				success: function(response){
					if (!response.success && response.error)
						alert("Erro ao processar solicitação. Erro: " + response.error);
					
					self.limparFormulario();					
					self.listar();
					self.exibirConteudo();
				}
			});
		},
		listar: function(){
			var self = this;
			
			self.exibirLoading();
			
			$.ajax({
				url: '/tecnospeed/controller/UsuarioController.php',
				data: {
					acao: 'listar'
				},
				method: 'POST',
				dataType: 'json',
				success: function(response){
					if (!response.success && response.error)
						alert("Erro ao processar solicitação. Erro: " + response.error);								
					
					self.montaTabela(response.data);
					self.exibirConteudo();
				}
			});
		},
		excluir: function(id){
			var self = this;
			
			self.exibirLoading();
			
			$.ajax({
				url: '/tecnospeed/controller/UsuarioController.php',
				data: {
					acao: 'excluir',
					id: id
				},
				method: 'POST',
				dataType: 'json',
				success: function(response){
					if (!response.success && response.error)
						alert("Erro ao processar solicitação. Erro: " + response.error);								
					
					self.montaTabela(response.data);
					self.exibirConteudo();
				}
			});
		},
		montaTabela: function(data){
			var self = this,
				html = '<tr><th>ID</th><th>Nome</th><th>E-mail</th><th></th></tr>';
			
			$.each(data, function(i,v){
				html += "<tr><td>"+v.id+"</td><td>"+v.nome+"</td><td>"+v.email+"</td><td><input type='button' class='btn_excluir' data-id='" + v.id + "' value='Excluir' /></td></tr>";
			});
			
			$("#table_usuario").html(html);
			
			$(".btn_excluir").click(function(){
				var id = $(this).data('id');
				
				self.excluir(id);
								
				return false;
			});
		},
		exibirLoading: function(){
			$("#loading_usuario").show();
			$("#content_usuario").hide();
		},
		exibirConteudo: function(){
			$("#loading_usuario").hide();
			$("#content_usuario").show();
		}
	};
	
	Usuario.init();
	
});