<?php

class Conexao {
	private $_servidor = 'localhost';
	private $_usuario = 'root';
	private $_senha = '';
	private $_db = 'tecnospeed';
	public $_conexao;
	
	public function __construct(){
		$this->_conexao = new mysqli($this->_servidor, $this->_usuario, $this->_senha, $this->_db);
	}
	
	public function select_one($query){
		if (empty($query))
			return false;
		
		$query = str_replace(';', '', $query);
		$query .= " LIMIT 1;";
		$result = $this->_conexao->query($query);
		
		return $result->fetch_array();
	}
	
	public function select($query){
		if (empty($query))
			return false;
		
		$data = array();
		
		$result = $this->_conexao->query($query);
		
		while ($dados = $result->fetch_array()){
			$data[] = $dados;
		}
		
		return $data;
	}
	
	public function query($query){
		return $this->_conexao->query($query);
	}
}