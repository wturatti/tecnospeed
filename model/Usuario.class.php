<?php
	include_once dirname(__FILE__) . "/../model/Conexao.class.php";
	
	class Usuario {
		public $id;
		public $nome;
		public $email;
		public $senha;
		private $_conexao;
		
		public function __construct(){
			$this->_conexao = new Conexao();
		}
		
		public function getId($id){
			if (empty($id))
				return false;
			
			$result = $this->_conexao->select_one("SELECT * FROM usuario WHERE id = " . $id . ";");
			$this->id = $result['id'];
			$this->nome = $result['nome'];
			$this->email = $result['email'];
			$this->senha = $result['senha'];
		}
		
		public function listar(){
			return $this->_conexao->select("SELECT * FROM usuario");
		}
		
		public function cadastrar($nome, $email, $senha){
			return $this->_conexao->insert(array('nome', 'email', 'senha'), array($nome, $email, $senha));
		}
		
		public function excluir($query){
			return $this->_conexao->query($query);
		}
		
		public function atualizar(){
		
		}
	}